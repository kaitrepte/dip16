# Authors
	- Kai Trepte
	- Sebastian Schwalbe
	- Simon Liebing
	- Wanja T. Schulze
	- Jens Kortus
	- Hemanadhan Myneni
	- Aleksei Ivanov
	- Susi Lehtola

# DIP16 data
	- Molecular and electronic geometries
	- Jupyter notebooks for data evaluation 

The repository complements our article 

'Chemical bonding theories as guides for self-interaction corrected solutions: multiple local minima and symmetry breaking'.
