import ipywidgets as widgets
from ase.atoms import Atoms 
from ase.io import read,write
from ase.visualize import view 
import nglview as nv 
from ase.io import read
from ipywidgets import Button, Dropdown, Text, FloatSlider, GridBox,  Layout
from glob import glob 
import numpy as np 
#from DIP14 import DIP14
from nglview.color import ColormakerRegistry as cm

__all__=['MyView']
# Add VESTA colors 
# - get color name from VESTA e.g. Cl #00FF00
# - replace # with 0x 
cm.add_scheme_func('my_vesta_colors','''
 this.atomColor = function (atom) {
     if (atom.element == "C") {
       return 0x804929 // C
     } else if (atom.element == "H") {
       return 0xFFFFFF
     } else if (atom.element == "S") {
       return 0xFFFF00
     } else if (atom.element == "N") {
       return 0x0000FF
     } else if (atom.element == "O") {
       return 0xFF0000
     } else if (atom.element == "F") {
       return 0x00FF00
     } else if (atom.element == "CL") {
       return 0x007F00
     }
 }
''')

dct_colors =  {'blue': [0, 1, 1],
                 'tomato' : [255/255, 99/255, 71/255],
                 'darkgreen' : [66/255, 106/255, 12/255],
                 'lightgreen' : [144/255,238/255,144/255],
                 'lightpink' : [255/255,182/255,193/255],
                 'violett' : [173/255, 149/255, 243/255],
                 'orange': [230/255, 107/255, 25/255],
                 'darkgrey' : [105/255, 105/255, 105/255]}




class MyView: 
    """
        Nuclei and FOD visualizer 
        Generate: view object 
        Populate: view object with ase atoms nuclei positions 
        Populate: view object with FODs as spheres, i.e., enable two small sized spheres
        References: 
            [1] https://wiki.fysik.dtu.dk/ase//_modules/ase/visualize/ngl.html
            [2] https://github.com/nglviewer/nglview/issues/785
    """
    def __init__(self,atoms): 
        # nglview (atoms)
        self.view = None 
        # dipole view object 
        self.view_dipole = None 
        # label view object 
        self.view_label = None 
        # fods_up view object 
        self.views_fods_up = None
        # fods_dn view object 
        self.views_fods_dn = None
        self.symbol_FOD_up = 'X'
        self.symbol_FOD_dn = 'He'
        self.radius_FOD = 0.1
        self.radius_dipole = 0.2 
        self.FODs_equal_digits = 3 
        
        self.atoms = atoms 
        self.read_structure(atoms)
        self.init_nuclei()
        self.init_FODs() 
        self.init_controls() 
        #self.display() 

        self.rot_x = 0
        self.rot_y = 0
        self.rot_z = 0 
        self.zoom = 0
        
    def read_structure(self,atoms=None): 
        
        self.nuclei = Atoms([a for a in atoms if a.symbol != self.symbol_FOD_up and a.symbol != self.symbol_FOD_dn])
        self.FODs_up = Atoms([a for a in atoms if a.symbol == self.symbol_FOD_up])
        self.pos_up = self.FODs_up.get_positions() 
        self.FODs_dn = Atoms([a for a in atoms if a.symbol == self.symbol_FOD_dn])
        #COM1 = (nuclei.positions.sum(axis=0)/len(nuclei.get_positions()))
        self.COM = self.nuclei.get_center_of_mass()
    def init_nuclei(self):
        self.view = nv.show_ase(self.nuclei)
        self.view.clear_representations()
        self.view.add_ball_and_stick(radiusType='radius',radiusScale=0.53,color_scheme='my_vesta_colors') 
        self.view.center()
      
    def init_FODs(self): 
        for a_i in self.FODs_up: 
            self.view.shape.add_sphere(a_i.position.tolist(), [144/255,238/255,144/255], self.radius_FOD) 
        for a_i in self.FODs_dn: 
            if a_i.position.round(self.FODs_equal_digits).tolist() not in self.pos_up.round(self.FODs_equal_digits).tolist(): 
                self.view.shape.add_sphere(a_i.position.tolist(), [255/255, 99/255, 71/255], self.radius_FOD)

    def init_controls(self): 
        # text boxes 
        self.t1 = Text(value='{}'.format(self.atoms.name), 
                  placeholder='Type something', 
                  description='name:', 
                  disabled=False )
        # sliders 
        self.s1 = FloatSlider(value=self.radius_dipole, 
                              min=0.0, 
                              max=1.5,
                              step=0.01, 
                              description='Dipole size')
        self.s2 = FloatSlider(value=0.53, 
                              min=0.0, 
                              max=4,
                              step=0.01, 
                              description='Nuclei size')
        # dropdown
        self.d1 = Dropdown(options=list(dct_colors.keys()),
                           value='lightgreen', 
                           description='Dipole color')
        # Buttons 
        # Screenshot
        self.b1 = Button(description='Screenshot')
        self.b1.on_click(self.on_click_screenshot)

        # Dipole
        self.b2 = Button(description='Dipole')
        self.b2.on_click(self.on_click_dipole)
        self.d1.observe(self.on_click_dipole)
        self.s1.observe(self.on_click_dipole)

        # Ball and Stick
        self.b3 = Button(description='Ball&Stick')
        self.b3.on_click(self.on_click_ball_and_stick)
        self.s2.observe(self.on_click_ball_and_stick)

        # Label 
        self.b4 = Button(description='Label')
        self.b4.on_click(self.on_click_label)
        self.t1.observe(self.on_click_label)

    def display(self):
        # view 
        #display(self.view)
        # text boxes 
        display(self.t1)
        # sliders
        display(self.s1)
        display(self.s2)
        # dropdown
        display(self.d1)
        # Buttons
        display(self.b1)
        display(self.b2)
        display(self.b3)
        display(self.b4)
        display(self.view)

    def on_click_screenshot(self,b):
        """ on click method to save screenshots
            References: 
            [1] http://nglviewer.org/nglview/latest/_api/nglview.widget.html?highlight=screenshot
        """
        self.view.download_image(filename='{}.png'.format(self.t1.value), 
                                 factor=4, 
                                 antialias=True, 
                                 trim=True, 
                                 transparent=False)

    def on_click_dipole(self,b):
        # https://hypejunction.github.io/color-wizard/
        if self.view_dipole is not None: 
            self.view_dipole.clear_representations()
        color = dct_colors[self.d1.value]
        DIP = self.atoms.dipole/0.208
        norm = np.linalg.norm(DIP)
        DIP = DIP + self.COM            
        self.view_dipole = self.view.shape.add_arrow(self.COM.tolist(), DIP.tolist(), color, self.s1.value)


    def on_click_ball_and_stick(self,b): 
        # change representation 
        self.view.clear_representations()
        self.view.add_ball_and_stick(color_scheme='my_vesta_colors',
                                     radiusScale=self.s2.value,radiusType='radius') 

    def on_click_label(self,b):
        if self.view_label is not None: 
            self.view_label.clear_representations()
        self.view_label = self.view.shape.add('text', 
                                                 self.COM.tolist(), 
                                                 [0,0,0], 
                                                 1, 
                                                 '{}'.format(self.t1.value))
    
    def rotate_view(self, x=0, y=0, z=0,degrees=True):
        radians = 1
        if degrees: radians = np.pi / 180
        self.view.control.spin([1, 0, 0], x*radians)
        self.view.control.spin([0, 1, 0], y*radians)
        self.view.control.spin([0, 0, 1], z*radians)
    
    def zoom_view(self,zoom=0):
        self.view.control.zoom(zoom)
